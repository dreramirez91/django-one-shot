from django.forms import ModelForm
from todos.models import TodoItem, TodoList


class TodoItemForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = ("task", "due_date", "is_completed", "list")
