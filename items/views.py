from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoItem
from .forms import TodoItemForm


def create_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            new_item = form.save()
            return redirect("todo_list_detail", id=new_item.list.id)
    else:
        form = TodoItemForm()
    context = {"form": form}
    return render(request, "items/createitem.html", context)


def edit_item(request, id):
    item_to_edit = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item_to_edit)
        if form.is_valid():
            edited_item = form.save()
            return redirect("todo_list_detail", id=edited_item.list.id)
    else:
        form = TodoItemForm(instance=item_to_edit)
    context = {"form": form}
    return render(request, "items/edititem.html", context) 
