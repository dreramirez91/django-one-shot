from django.forms import ModelForm
from .models import TodoItem, TodoList


class TodoListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = ("name",)
