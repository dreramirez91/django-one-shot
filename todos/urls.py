from django.urls import path
from .views import (
    list_lists,
    list_details,
    create_list,
    edit_list,
    delete_list,
)
from items.views import create_item, edit_item

urlpatterns = [
    path("", list_lists, name="todo_list_list"),
    path("<int:id>/", list_details, name="todo_list_detail"),
    path("create/", create_list, name="todo_list_create"),
    path("<int:id>/edit/", edit_list, name="todo_list_update"),
    path("<int:id>/delete/", delete_list, name="todo_list_delete"),
    path("items/create/", create_item, name="todo_item_create"),
    path("items/<int:id>/edit/", edit_item, name="todo_item_update"),
]
