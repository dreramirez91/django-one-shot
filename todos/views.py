from django.shortcuts import render, get_object_or_404, redirect
from .models import TodoList, TodoItem
from .forms import TodoListForm


def list_lists(request):
    lists = TodoList.objects.all()
    context = {"lists": lists}
    return render(request, "todos/list.html", context)


def list_details(request, id):
    list_details = get_object_or_404(TodoList, id=id)
    context = {"list_details": list_details}
    return render(request, "todos/details.html", context)


def create_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            new_list = form.save()
            return redirect("todo_list_detail", id=new_list.id)
    else:
        form = TodoListForm()
    context = {"form": form}
    return render(request, "todos/create.html", context)


def edit_list(request, id):
    list_to_edit = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list_to_edit)
        if form.is_valid():
            new_list = form.save()
            return redirect("todo_list_detail", id=new_list.id)
    else:
        form = TodoListForm(instance=list_to_edit)
    context = {"form": form}
    return render(request, "todos/edit.html", context)


def delete_list(request, id):
    list_to_delete = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        list_to_delete.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")
